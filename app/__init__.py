import os

from flask import Flask
from dotenv import load_dotenv

import dns.resolver
dns.resolver.default_resolver = dns.resolver.Resolver(configure=False)
dns.resolver.default_resolver.nameservers = ['8.8.8.8']

# os.getcwd() gets the current working directory of a file
# Since we are running main.py, os.getcwd() points out to the root of the project
app = Flask(__name__)
load_dotenv(str(os.getcwd()) + "/.env")
# Syntax: os.getenv(<ENVIRONMENT_VARIABLE_NAME>)

from app import routes












